import React from "react";
import { getMdxNode, getMdxPaths } from "next-mdx/server";
import { useHydrate } from "next-mdx/client"
import { mdxComponents } from "../../components/mdx-components";


export default function PostPage({ page }) {

const content = useHydrate(page,{
    components : mdxComponents
})
console.log(page)
  return <div className="site-container">
      <article className="prose" >
          <h1 className="text-2xl font-bold">{page.frontMatter.title}</h1>
          <p>{page.frontMatter.excerpt}</p>
          <hr/>
          <p>{content}</p>
      </article>
  </div>;
}

export async function getStaticPaths() {
  return {
    paths: await getMdxPaths("post"),
    fallback: false,
  };
}

export async function getStaticProps(context) {
  const page = await getMdxNode("post", context);

  return {
    props: {
      page,
    },
  };
}
